diff --git a/diff.html b/diff.html
index eb7db81..516ec2d 100644
--- a/diff.html
+++ b/diff.html
@@ -249,10 +249,13 @@ teljesítményük alapján a legjobban megérdemlik.</p>
 <li>Esti tagozat szalagavatója</li>
 <li>Szalagavató</li>
 <li>Karácsonyi műsor</li>
-<li>Projekt napok <span class="diff-del">- Kampány</li>
-<li>Teapub</span>- UNESCO</li>
-<li><span class="diff-add">Teapub</li>
-<li>Kampány</span>- Esti tagozat ballagása</li>
+<li>Projekt napok</li>
+<li><span class="diff-del">Kampány</span></li>
+<li><span class="diff-del">Teapub</span></li>
+<li>UNESCO</li>
+<li><span class="diff-add">Teapub</span></li>
+<li><span class="diff-add">Kampány</span></li>
+<li>Esti tagozat ballagása</li>
 <li>Ballagás</li>
 <li>Öregdiáktalálkozó</li>
 <li>Tojásbuli</li>
