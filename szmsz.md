<!-- # A Szent László Gimnázium Stúdiójának működési szabályzata -->

## 1) A Stúdió felépítése és működése

### 1. Mi a Stúdió?

A Stúdió egy diákokból álló, önkéntes szervezet, amely az iskolai
rendezvényeken a hang- és fénytechnikát szolgáltatja és kezeli. Emellett
az iskolarádiót és az iskolacsengőt üzemelteti.

### 2. Szerepkörök

```Betanuló => Technikus => Stábvezető => Stúdiót vezető diák```

**Stúdiót vezető diák**: Minden év elején választással vagy
kijelöléssel dől el. Mandátuma egy évre szól.

*Feladata*: A Stúdió vezetése, feladatköreinek összefogása és
kapcsolattartás a rendezvények szervezőivel.

**Stábvezető**: A Stúdiót vezető diák jelöli ki a technikusok
közül. A létszámtól függően, a hatékonyabb munkavégzés érdekében,
stábokba vannak osztva a betanulók.

*Feladata*: A betanulók tanítása és munkájuk felügyelete.

**Technikus**: A második stúdiós évük megkezdésekor válnak a
betanulók technikussá.

*Feladata*: A rendezvények lebonyolításában segédkezik.

**Betanuló**: A újonnan felvett emberek kerülnek ebbe a
szerepkörbe, általában gólyák. Csak magasabb beosztású stúdiós
felügyelete mellett dolgozhatnak.

*Feladata*: A hang- és fénytechnikai eszközök kezelésének
elsajátítása.

**Stúdiós tanár**: Összeköttetést biztosít a rendezvényszervezőkkel,
tantestületi fórumokon képviseli a stúdiót.
Hiányzásokat és a közösségi szolgálatot igazolja.

### 3. Kulcsok

Korlátozott számú kulccsal rendelkezik a Stúdió, melyek a stúdiót vezető
diáknál, a Stúdiós tanárnál, stábvezetőknél, néhány technikusnál és a
portán találhatóak. A végzős stúdiósók a ballagás után, annak a diáknak
kötelesek továbbadni a kulcsukat, a betanulók vagy technikusok közül,
akik szerintük, teljesítményük alapján a legjobban megérdemlik.

### 4. Programok, rendezvények
-   Évnyitó
-   Gólyabál
-   Szülői tájékoztató
-   Nyíltnapok
-   Esti tagozat szalagavatója
-   Szalagavató
-   Karácsonyi műsor
-   Projekt napok
-   UNESCO
-   Teapub
-   Kampány
-   Esti tagozat ballagása
-   Ballagás
-   Öregdiáktalálkozó
-   Tojásbuli
-   Sportnap
-   Gyereknap
-   Évzáró
-   *Ezek mellett kisebb szezonális rendezvények, melyeket legalább egy
    héttel előtte egyeztettek.*

## 2) A Stúdió jogai és kötelességei

*A Stúdió -a stúdiós tanár és a DÖK véleményének kikérésével- dönt a
működéséről.*

### 1. A Stúdió jogai

*A Stúdiónak joga van, (hogy)...*

1.  a rendezvényekre kért hangosítási igényekről és a program
    menetrendjéről a Stúdiót vezető diákot minimum egy héttel az esemény
    előtt tájékoztassák, a Google űrlap kitöltésével.

2.  a rendezvényhez szükséges hang- és fénytechnikai igényekről
    forgatókönyvet és megfelelő tájékoztatást kérni.

3.  egy időpontot jelöljön ki, amikorra az osztályok közötti programok
    (Gólyabál, UNESCO, Kampány) résztvevői leadják, külön pendrive-on az
    egyértelműen elnevezett, összevágott zenéket, digitális- illetve három
    nyomtatott forgatókönyvet, amin ki vannak emelve a zenei, hang- és
    fénytechnikai igények, és a stúdiót segítő diák neve.

4.  minden versenyző osztályból egy (UNESCON-n lehet 2) embert maga
    mellé rendeljen, aki tisztában van az előadás menetével.

5.  kampányon az osztálytáncok zenéit pendrive-on, a többi program
    zenéit lehetőleg 3,5mm-es jack csatlakozóval ellátott telefonon
    kérje a kampányolóktól.

6.  a gólyabálon és UNESCO-n egy szavazattal rendelkezzen.

7.  a Kampányon panaszt tehessen az osztályokra a HATOK felé.

8.  szükség esetén (pl.: délutáni program, nagyobb előkészítést igénylő
    program) a díszterem egy kulcsát kezelje.

9.  a stúdiós felszerelés kizárólagos kezeléséhez.

10. az érettségi időszakon kívül, a szünetekben az iskola színvonalához
    méltó zenét játszon le a hangszórókon.

11. minimum egy fővel képviseltethesse magát a DÖK táborban és a Stúdiót
    érintő fórumokon.

12. az iskolaidőn kívüli munkákért közösségi szolgálati órákat kapjanak
    a tagok.

13. ha egy program az előre bejelentett időn túl tart először a program
    rendezőjét figyelmeztesse, és 15 perc elteltével a munkát
    megtagadja.

14. olyan körülmények között dolgozzon, amelyek
    megfelelőek a berendezés optimális, előírt működtetésére.
    (Pl. eső, vagy kevesebb mint 10°C nem várható.)
    Ebben a kérdésben a stúdiót vezető diák és/vagy az iskolavezetőség dönt.
    Ha ott nem is, de köteles egy másik, megfelelő
    helyszínen hangosítani, amennyiben a fentebb leírt feltételek teljesülnek.

### 2. Egyéb rendelkezések

1.  A stúdiósok csak és kizárólag a stúdiós felszerelések pakolásáért,
    szállításáért és üzemeltetéséért felelősek.

2.  A stúdiós tanár igazolja le a munka miatti hiányzást.

3.  A stúdiós munkákat, a stúdiós tanár és/vagy a szaktanár engedélyével
    iskolaidőben is elvégezhetik a stúdiósok.

4.  A bemondani kívánt közleményeket a HATOK a Stúdió kapcsolattartóján
    keresztül eljuttatja a Stúdiónak.

5. Ezt a dokumentumot minden iskolapolgár számára elérhetővé kell tenni.

### 3. A Stúdiósok kötelességei

1.  Az előre vállalt munkát legjobb tudásuk szerint teljesíteni.

2.  Az elvégzendő munkákat a stúdiós tanár és a stúdiót vezető diák
    közösen egyezteti és ossza be a megfelelő létszámú embert.

3.  Órákról való stúdiós munka miatti hiányzás esetén minden stúdiós
    köteles az osztályfőnökével és/vagy a szaktanárral egyeztetni
    legkésőbb az óra előtt. Ezeket az erre kifejlesztett űrlapban
    feltűntetni.

4.  Minden osztálynak egyenlő feltételeket kell biztosítani a
    programokon, amennyiben betartották a fentebb leírt kéréseket
    (pendrive, forgatókönyv, határidő betartása).

5.  Előre bejelentett dolgozatot kötelesek a tagok megírni az előre
    bejelentett időpontban.

6.  Az események záróidőpontjainak betartása.

7.  Az iskola színvonalának megfelelő zenék lejátszása az iskolarádióban
    és az eseményeken.

8.  Működőképes rendszer kiépítése kezdés előtt fél/egy órával,
    az esemény méretétől és a rendszer komplexitásától függően.

9.  A tantestület tagjainak és a DÖK vezetőinek biztosítani az iskolai
    eseményekről való tájékoztatást az iskolarádióban.

## Záradék
*A stúdió szervezeti és működési szabályzatának
módosítására, kiegészítésére javaslatot tehet a Hatok, a DÖMST, a
stúdiós tanár, valamint a stúdió tagjai. Ezt kötelesek megtárgyalni a
fent említettek, és 2/3 szavazattöbbség esetén a szabályzatot
módosítani. Ezt a szabályzatot a DÖK-nek, a stúdiós tanárnak, illetve a
Stúdiót vezető diáknak kell elfogadnia, hogy hiteles legyen.*

Készítették:
- Mester-Szabó Eszter, Lupu-Maár Endre, Pálfi Bence, Gelencsér Bálint, *Balatonszepezd, 2019.08.28.*
- Varga Benedek, Soós Csaba, *Budapest, 2023.?.?.*
